# Add your colors here
# Example: rainbow colors 🌈
colors = ['red', 'orange', 'yellow', 'green', 'blue', 'violet']


# Offset of the first color
start = 0.0
# Offset of the last color
stop = 100.0

i = 0
step = (stop - start) / colors.length

text = []

for color in colors
  text.push "%s %f%% %f%%" % [color, start + i * step, start + (i + 1) * step]
  i = i + 1
end

# Try replacing this with "radial-gradient(%s)"
puts "linear-gradient(to bottom, %s)" % text.join(', ')
