![The text “Doggo ipsum doge waggy wags heckin angery woofer.” colored horizontally with multiple color. The background is a light green tone.](https://pbs.twimg.com/media/FX0WEVUUYAAIoXa?format=png&name=900x900)

# Hard CSS gradients

CSS gradients not only support the traditional gradual transitions but also [hard transitions](https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Images/Using_CSS_gradients#creating_hard_lines). This project is a HTML page to demo hard line gradients for different gradient types.

You can also use `ruby colors.rb` to generate a `linear-gradient` with different colors.
